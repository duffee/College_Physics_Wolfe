## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Substructure of Nucleus)
## Date(March 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('31.3')
## Problem1('5')
## MO(1)
## KEYWORDS('density','nuclear')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$density_nuclear = NumberWithUnits( 2.3E17, 'kg/m^3');
$density_water = NumberWithUnits( 1000, 'kg/m^3');

$mass = NumberWithUnits( random(2, 2.5, 0.05)*1E17, 'kg');

$volume_water = NumberWithUnits( $mass /$density_water, 'm^3');
$length_water = NumberWithUnits( ($volume_water)**Real(1/3), 'm');
ANS( $length_water->cmp );

$length_nuclear = NumberWithUnits( sprintf("%0.2f", ($mass/$density_nuclear)**Real(1/3)), 'm');


Context()->texStrings;
BEGIN_TEXT

Calculate the side lengths of a \($mass\) cube of water at \(4^{\circ}\rm C\).
(This mass at nuclear density would make a cube about \($length_nuclear\) on a side.)
$PAR
\( l = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
What is the density of water at the given temperature?
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

The volume of the cube is
\( \displaystyle V = \frac{m}{\rho} = \frac{$mass}{$density_water}
  = $volume_water
\)
$PAR
The length of a side of a cube is
\( l = \sqrt[3]{V} = \sqrt[3]{$volume_water} = $length_water \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
