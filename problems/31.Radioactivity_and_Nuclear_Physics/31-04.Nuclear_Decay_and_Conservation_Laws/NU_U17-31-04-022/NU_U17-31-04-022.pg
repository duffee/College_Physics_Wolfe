## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
#
# TODO make masses into MathObjects
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Nuclear Decay and Conservation Laws)
## Date(May 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('31.4')
## Problem1('38')
## MO(1)
## RESOURCES('Half_Lives_Appendix.png')
## KEYWORDS('energy','mass','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$mass_Sr = 89.907738;
$mass_Y = 89.907152;

$a = Real(90);
$b = Real(39);
$c = Real(51);
ANS( $a->cmp );
ANS( $b->cmp );
ANS( $c->cmp );

$delta_mass = ($mass_Sr - $mass_Y);
$energy = Real( $delta_mass * 931.5 ); # MeV
ANS( $energy->cmp );


Context()->texStrings;
BEGIN_TEXT

Click
\{ htmlLink( alias('Half_Lives_Appendix.png'), "here", "TARGET='_blank'" ) \}
to see a table of the elements and their atomic masses, half-lives, and percent abundances.

$PAR
Fill in the missing fields for the equation describing the \(\beta^{-}\) decay
of \(^{90}\rm Sr\), a major waste product of nuclear reactors.

$PAR
$BCENTER
\(^{90}_{38}\textrm{Sr}_{52} \ \longrightarrow \ ^{a}_{b}\textrm{Y}_{c} \ + \ \beta^{-} \ + \ \overline{\nu_e}\)
$ECENTER

$PAR
\(a\) = \{ans_rule(10)\}
$PAR
\(b\) = \{ans_rule(10)\}
$PAR
\(c\) = \{ans_rule(10)\}

END_TEXT
BEGIN_HINT
Recall that, in \(\beta^{-}\) decay, the
$PAR
<b>i.</b> charge,
$PAR
<b>ii.</b> electron family number,
$PAR
<b>iii.</b> and number of nucleons
$PAR
are all conserved quantities.
END_HINT
BEGIN_TEXT

$PAR
b) Calculate the energy released in the decay.
$PAR
\{ans_rule(40)\} \(\textrm{MeV}\)

END_TEXT


BEGIN_HINT
Recall the formula for the energy released in \(\beta^{-}\) decay.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\(^{90}_{38}\textrm{Sr}_{52} \ \longrightarrow \ ^{$a}_{$b}\textrm{Y}_{$c} \ + \ \beta^{-} \ + \ \overline{\nu_e}\)
$PAR
b)
$PAR
\( \Delta m = m_{\textrm{Sr}} - m_{\textrm{Y}}
  = $mass_Sr - $mass_Y
  = $delta_mass \rm \ u
\)
$PAR
\( E = \Delta m \times 931.5 \rm \ MeV = $delta_mass \ u \times 931.5 \ MeV = $energy \ MeV \)

END_SOLUTION


ENDDOCUMENT();
