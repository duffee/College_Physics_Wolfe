## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Nuclear Decay and Conservation Laws)
## Date(March 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('31.4')
## Problem1('18')
## MO(1)
## Static(1)
## KEYWORDS('mass','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'MathObjects.pl',
);

TEXT(beginproblem());
Context("Numeric");

$showPartialCorrectAnswers = 1;
$showHint = 3;


$a = Real(40);
$b = Real(20);
$c = Real(20);
ANS( $a->cmp );
ANS( $b->cmp );
ANS( $c->cmp );


Context()->texStrings;
BEGIN_TEXT

Fill in the missing fields for the equation describing the \(\beta^{-}\) decay of \(^{40}\rm K\),
a naturally-occurring rare isotope of potassium responsible for some of our exposure to background radiation.
$PAR
$BCENTER
\(\rm ^{40}_{19} K_{21} \ \longrightarrow \ ^a_b Ca_c {+} \ \beta^{-} {+} \ \overline{\nu_e}\)
$ECENTER

$PAR
\(a\) = \{ans_rule(10)\}
$PAR
\(b\) = \{ans_rule(10)\}
$PAR
\(c\) = \{ans_rule(10)\}

END_TEXT


BEGIN_HINT
Recall that, in \(\beta^{-}\) decay, the
$PAR
<b>i.</b> charge,
$PAR
<b>ii.</b> electron family number,
$PAR
<b>iii.</b> and number of nucleons
$PAR
are all conserved quantities.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\(\rm ^{40}_{19} K_{21} \ \longrightarrow \ ^{$a}_{$b} Ca_{$c} {+} \ \beta^{-} {+} \ \overline{\nu_e}\)

END_SOLUTION


ENDDOCUMENT();
