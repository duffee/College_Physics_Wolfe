## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Nuclear Decay and Conservation Laws)
## Date(March 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('31.4')
## Problem1('21')
## MO(1)
## Static(1)
## KEYWORDS('mass','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'MathObjects.pl',
);

TEXT(beginproblem());
Context("Numeric");

$showPartialCorrectAnswers = 1;
$showHint = 3;


$a = Real(206);
$b = Real(82);
$c = Real(124);
ANS( $a->cmp );
ANS( $b->cmp );
ANS( $c->cmp );


Context()->texStrings;
BEGIN_TEXT

Fill in the missing fields for the equation describing the \(\alpha\) decay of \(^{210}\textrm{Po}\),
the isotope of polonium in the decay series of \(\,^{238}\textrm{U}\) that was discovered by the Curies.
A favorite isotope in physics labs, it has a short half-life and decays to a stable nuclide.
$PAR
$BCENTER
\(\rm ^{210}_{84} Po_{126} \ \longrightarrow \ ^{a}_{b} Pb_{c} + \ ^4_2 He_2\)
$ECENTER

$PAR
\(a\) = \{ans_rule(10)\}
$PAR
\(b\) = \{ans_rule(10)\}
$PAR
\(c\) = \{ans_rule(10)\}

END_TEXT


BEGIN_HINT
Recall that, in \(\alpha\) decay, the
$PAR
<b>i.</b> charge,
$PAR
<b>ii.</b> electron family number,
$PAR
<b>iii.</b> number of nucleons,
$PAR
<b>iv.</b> and number of protons and neutrons
$PAR
are all conserved quantities.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\(\rm ^{210}_{84} Po_{126} \ \longrightarrow \ ^{$a}_{$b} Pb_{$c} + \ ^4_2 He_2\)

END_SOLUTION


ENDDOCUMENT();
