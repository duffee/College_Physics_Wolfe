## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
#
# TODO make masses into MathObjects
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Nuclear Decay and Conservation Laws)
## Date(May 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('31.4')
## Problem1('43')
## MO(1)
## RESOURCES('Half_Lives_Appendix.png')
## KEYWORDS('energy','mass','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$mass_N = 15.000108;
$mass_O = 15.003065;

$a = Real(15);
$b = Real(7);
$c = Real(8);
ANS( $a->cmp );
ANS( $b->cmp );
ANS( $c->cmp );

$delta_mass = ($mass_O - $mass_N);
$energy = Real( $delta_mass * 931.5 ); # MeV
ANS( $energy->cmp );


Context()->texStrings;
BEGIN_TEXT

Click
\{ htmlLink( alias('Half_Lives_Appendix.png'), "here", "TARGET='_blank'" ) \}
to see a table of the elements and their atomic masses, half-lives, and percent abundances.

$PAR
Fill in the missing fields for the equation describing the electron capture of \(^{15}\rm O\).

$PAR
$BCENTER
\(^{15}_{8}\textrm{O}_{7} \ + \ e^{-} \ \longrightarrow \ ^{a}_{b}\textrm{N}_{c} \ + \ \nu_e\)
$ECENTER

$PAR
\(a\) = \{ans_rule(10)\}
$PAR
\(b\) = \{ans_rule(10)\}
$PAR
\(c\) = \{ans_rule(10)\}

END_TEXT
BEGIN_HINT
Recall that, in electron capture, the
$PAR
<b>i.</b> charge,
$PAR
<b>ii.</b> electron family number,
$PAR
<b>iii.</b> and number of nucleons
$PAR
are all conserved quantities.
END_HINT
BEGIN_TEXT

$PAR
b) Calculate the energy released.
$PAR
\{ans_rule(40)\} \(\textrm{MeV}\)

END_TEXT


BEGIN_HINT
Recall the formula for the energy released in electron capture.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\(^{15}_{8}\textrm{O}_{7} \ + \ e^{-} \ \longrightarrow \ ^{$a}_{$b}\textrm{N}_{$c} \ + \ \nu_e\)
$PAR
b)
$PAR
\( \Delta m = m_{O} - m_{N}
  = $mass_O - $mass_N
  = $delta_mass \rm \ u
\)
$PAR
\( E = \Delta m \times 931.5 \rm \ MeV = $delta_mass \ u \times 931.5 \ MeV = $energy \ MeV \)

END_SOLUTION


ENDDOCUMENT();
