## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Half-Life and Activity)
## Date(May 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('31.5')
## Problem1('54')
## MO(1)
## KEYWORDS('half-life','mass','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$half_life = Real(5730); # years for Carbon 14

$N_avogadro = Real(6.022E23);

$activity = sprintf("%0.2f", random(1,1.5,0.05));
$abundance = Real(1.3E-12);
$half_life_hours = $half_life *365.25*24;
$A = 12;
$mass_g = sprintf("%0.2f", random(1,1.5,0.05));

$particles = Real($abundance * ($mass_g/$A) * $N_avogadro);
$initial_activity = Real( ln(2) * $particles / $half_life_hours ); # decays/hour
$time = Real( -$half_life / ln(2) * ln($activity/$initial_activity) );
ANS( $time->cmp );


Context()->texStrings;
BEGIN_TEXT

Click
\{ htmlLink( alias('Half_Lives_Appendix.png'), "here", "TARGET='_blank'" ) \}
to see a table of the elements and their atomic masses, half-lives, and percent abundances.

$PAR
A tree falls in a forest.
How many years must pass before the \(\,^{14}\rm C\) activity in \($mass_g \ \rm g\)
of the tree's carbon drops to \($activity\) decays per hour?
Take the percent abundance of \(\,^{14}\rm C\) to be \(1.30 \times 10^{-10} \, \%\).
$PAR
\{ans_rule(40)\} years

END_TEXT


BEGIN_HINT
Recall the formula for exponential decay, expressed in terms of activities. Further recalling Avogadro's number and the definition of molar mass, can you first solve for the initial activity of the carbon-14 in the tree?
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle N = \frac{m}{A} \ \textrm{abundance} \ N_a
  = \frac{$mass_g \ \rm g}{$A} ($abundance)($N_avogadro)
  = $particles
\)
$PAR
\( \displaystyle R_0 = \frac{ \ln(2) N }{ t_{1/2} }
  = \frac{ (0.693)($particles) }{ $half_life_hours \ \rm hours }
  = $initial_activity \ \textrm{decays per hour}
\)
$PAR
\( \displaystyle t = \frac{ -t_{1/2}}{ \ln 2 } \ \ln \left( \frac{R}{R_0} \right)
  = \frac{ - ($half_life \ \textrm{years})}{ \ln 2 } \ \ln \left( \frac{ $activity }{ $initial_activity } \right)
  = $time \ \textrm{years}
\)

END_SOLUTION


ENDDOCUMENT();
