## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up and added solution
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Half-Life and Activity)
## Date(February 2024)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## MO(1)
## Section1('31.5')
## Problem1('63')
## KEYWORDS('half-life','mass','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$mass = random(4000, 4500, 50); # kg
$ukg = 1 / 1.6605E-27;
$thalf238 = 4.468E9 * 3.156E7; # s
$decay = 4.27; # MeV
$Ro = Real(0.693 * $mass * $ukg / (238.050784 * $thalf238) / 3.7E10); # Ci
$Energy = Real(5E10 * $decay * 1.602E-19 * 8.64E4 * (1/4.186)); # cal/day

ANS( $Ro->cmp );
ANS( $Energy->cmp );


Context()->texStrings;
BEGIN_TEXT

Click
\{ htmlLink( alias('Half_Lives_Appendix.png'), "here", "TARGET='_blank'" ) \}
to see a table of the elements and their atomic masses, half-lives, and percent abundances.

$PAR
Large amounts of depleted uranium \(^{238}\textrm{U}\) are available as a by-product
of uranium processing for reactor fuel and weapons. Uranium is very dense and makes good
counter-weights for aircraft.
Suppose you have a \($mass \, \textrm{kg}\) block of \(^{238}\textrm{U}\).

$PAR
a) Find its activity.
$PAR
\{ans_rule(40)\} \(\textrm{Ci}\)

END_TEXT

BEGIN_HINT
Given, in the linked table, the mass of a single uranium-238 atom, can you first solve for the number of radioactive nuclei?
END_HINT

BEGIN_TEXT

$PAR
b) How many calories per day are generated by thermalization of the decay energy?
The energy released per decay is \(4.27 \, \textrm{MeV}\).

$PAR
\{ans_rule(40)\} \(\frac{\textrm{cal}}{\textrm{day}}\)

END_TEXT


BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

a) $PAR
\( \displaystyle R_0 = \frac{ln(2) N}{t_{1/2}}
  = \frac{0.693 \times $mass \times $ukg}{238.050784 \times $thalf238 \times 3.7\times 10^{10}}
  = $Ro \ \textrm{Ci}
\)

$PAR b) $PAR
\( E = 5\times 10^{10} \cdot $decay \cdot 1.602\times 10^{-19} \cdot 8.64\times 10^4 \div 4.186
  = $Energy \ \rm \frac{cal}{day}
\)

END_SOLUTION


ENDDOCUMENT();
