## DESCRIPTION
# originally written by Caroline Promnitz and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Nuclear)
## DBchapter(Radioactivity and Nuclear Physics)
## DBsection(Half-Life and Activity)
## Date(February 2024)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## MO(1)
## Section1('31.5')
## Problem1('60')
## KEYWORDS('half-life','nuclear','radiation')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$c = 3.7*10**10;           #Becquerel to curie.

$activity_1uCi = sprintf("%0.2f", random(3, 3.5, 0.05));
$activity_1 = ($activity_1uCi * $c * 60)*1E-6;
$activity_observed = random(1100, 1600, 50);
$half_life = 138.39; # days
$time = random(100, 125, 5); # days

$Ro = ($uCi * 1E-6) * $BqCi;
$days = random(110, 125, 1);
$activity_2 = ($activity_1) * exp(-ln(2) * $time / $half_life);

$ratio = Real($activity_observed / $activity_2);
ANS( $ratio->cmp );


Context()->texStrings;
BEGIN_TEXT

Click
\{ htmlLink( alias('Half_Lives_Appendix.png'), "here", "TARGET='_blank'" ) \}
to see a table of the elements and their atomic masses, half-lives, and percent abundances.

$PAR

The \(\,^{210}\textrm{Po}\) source used in a physics laboratory is labeled as having an activity of
\($activity_1uCi \ \mu \textrm{Ci}\) on the date it was prepared.
A student measures the radioactivity of this source with a Geiger counter and observes \($activity_observed\) counts per minute.
She notices that the source was prepared \($time\) days before her lab.
What fraction of the decays is she observing with her apparatus?

$PAR

\{ans_rule(40)\}

END_TEXT


BEGIN_HINT
Can you first rearrange the formula for exponential decay, expressed in terms of activities, to solve for the actual diminished activity of the polonium sample?
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

Polonium has a half-life of \($half_life\) days.
The activity after \($time\) days is
\( \displaystyle R = R_0 \, e^{- \lambda t} = R_0 \, e^{\frac{-0.693}{$half_life} $time } = $activity_2 \ \textrm{Bq} \)
$PAR
Convert the observed activity to counts per second and divide by the predicted activity to get the fraction of decays observed.
\( \displaystyle \frac{$activity_observed \div 60}{$activity_2} = $ratio \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
