##DESCRIPTION
# originally written by Kyle Winch and Sara Hesse, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Electricity)
## DBchapter(Electric Current, Resistance, and Ohm's Law)
## DBsection(Current)
## Date(May 2024)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText('College Physics')
## AuthorText('Urone et al')
## EditionText('2015')
## MO(1)
## Section('20.1')
## Problem('001')
## KEYWORDS('charge','current','ampere')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$C = NumberWithUnits( random(1, 9, 1), 'C' );
$h = NumberWithUnits( random(1, 9, 1), 'h' );
$current = NumberWithUnits( $C / ($h * Real(3600)), 'A');

ANS( $current->cmp );


Context()->texStrings;
BEGIN_TEXT

What is the current in milliamperes produced by the solar cells of a pocket calculator
through which \($C\) of charge passes in \($h\)?
$PAR
\( I = \) \{ans_rule(40)\}

END_TEXT


BEGIN_HINT
Remember to make proper unit conversions.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle I = \frac{Q}{t}
  = \frac{$C}{$h \times \frac{3600 \, \mathrm{s}}{\mathrm{h}}}
  = $current
\)

END_SOLUTION


COMMENT('Uses NumberWithUnits - mA available in pg v2.18');
ENDDOCUMENT();
