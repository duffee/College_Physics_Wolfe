##DESCRIPTION
# originally written by Kyle Winch and Sara Hesse, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## insert description here
##ENDDESCRIPTION

## DBsubject(Electricity)
## DBchapter(Electric Current, Resistance, and Ohm's Law)
## DBsection(Ohm's Law: Resistance and Simple Circuits)
## Date(May 2024)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText('College Physics')
## AuthorText('Urone et al')
## EditionText1('2015')
## MO(1)
## Section('20.2')
## Problem('003')
## KEYWORDS('current','resistance','voltage')


DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$V = NumberWithUnits( random(10.2, 11.8, 0.04), 'V' );
$I = NumberWithUnits( random(123, 168, 9), 'A' );
$R = Real( $V->value / $I->value ); # ohm

ANS( $R->cmp );


Context()->texStrings;
BEGIN_TEXT

What is the effective resistance of a car's starter motor when \($I\) flows through it
as the car battery applies \($V\) to the motor?
$PAR
\( R = \) \{ans_rule(40)\} \(\Omega\)

END_TEXT


BEGIN_HINT
Considering the magnitude of current, would you expect the magnitude of the resistance to be large or small?
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle R = \frac{V}{I}
  = \frac{$V}{$I}
  = $R \, \Omega
\)

END_SOLUTION


ENDDOCUMENT();
