## DESCRIPTION
# originally written by Caroline Promnitz and Sara Hesse, Brock University 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Waves)
## DBchapter(Physics of Hearing)
## DBsection(Sound Interference Resonance)
## Date(November 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('17.5')
## Problem1('38')
## KEYWORDS('frequency')

DOCUMENT();
loadMacros(
  "PGstandard.pl",
  "parserNumberWithUnits.pl",
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$f1 = NumberWithUnits( random(262, 265, 0.1), 'Hz');
$f2 = NumberWithUnits( random(266, 268, 0.2), 'Hz');
$fb = abs($f1-$f2);

ANS( $fb->cmp );


Context()->texStrings;
BEGIN_TEXT

A "showy" custom-built car has two brass horns that are supposed to produce the
same frequency but actually emit \($f1\) and \($f2\).
What beat frequency is produced?
$PAR
\(f_b = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall that a beat is created by alternating destructive and constructive interference.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\[ f_b = | f_1 - f_2 | = | $f2 - $f1 | = $fb \]

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
