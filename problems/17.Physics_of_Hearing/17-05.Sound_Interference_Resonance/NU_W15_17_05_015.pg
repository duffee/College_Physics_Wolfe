## DESCRIPTION
# originally written by Caroline Promnitz and Sara Hesse, Brock University 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Waves)
## DBchapter(Physics of Hearing)
## DBsection(Sound Interference Resonance)
## Date(November 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('17.5')
## Problem1('53')
## KEYWORDS('frequency','length','speed')

DOCUMENT();
loadMacros(
  "PGstandard.pl",
  "parserNumberWithUnits.pl",
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$L = NumberWithUnits( random(0.335, 0.359, 0.01), 'm');
$f1 = NumberWithUnits( random(255, 257, 1), 'Hz');
$vw = 331;
$T1 = 273.15;
$T = NumberWithUnits( (16 * ($L->value * $f1->value / $vw)**2 * $T1) - $T1, 'degC');
$Lb = $L * Real(3);

ANS( $T->cmp );
ANS( $Lb->cmp );


Context()->texStrings;
BEGIN_TEXT

a) Students in a physics lab are asked to find the length of an air column in a tube
closed at one end that has a fundamental frequency of \($f1\). They hold the tube
vertically and fill it with water to the top, then lower the water while a \($f1\) tuning
fork is rung and listen for the first resonance.
$PAR
What is the air temperature if the resonance occurs for a length of \($L\)?
$PAR
\( T = \) \{ans_rule(15)\}

$PAR
b) At what length will they observe the second resonance (first overtone)?
$PAR
\( l = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall that first resonance is fundamental.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

With \( l = $L \) and \(f = $f1\),
$PAR
\( \displaystyle T = \frac{4^2 l^2 f^2 T_{0^{\circ} \rm C} }{v}
  = \frac{ 16 \, ($L)^2 ($f1)^2 \, $T1 \ \rm K }{ $vw }
  = $T
\)

$PAR
(b)
The second resonance occurs at \( 3 l = $Lb \).

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT()
