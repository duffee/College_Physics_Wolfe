## DESCRIPTION
# originally written by Caroline Promnitz and Sara Hesse, Brock University 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Waves)
## DBchapter(Physics of Hearing)
## DBsection(Sound Interference Resonance)
## Date(November 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('17.5')
## Problem1('47')
## KEYWORDS('frequency','length','speed')

DOCUMENT();
loadMacros(
  "PGstandard.pl",
  "parserNumberWithUnits.pl",
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$f1 = NumberWithUnits( random(170, 189, 1), 'Hz');
$f2 = NumberWithUnits( random(350, 369, 1), 'Hz');
$f = $f2-$f1;
$vw = NumberWithUnits( random(342, 344, 1), 'm*s^-1');
$L = NumberWithUnits( $vw / (Real(2) * $f), 'm');

ANS( $L->cmp );


Context()->texStrings;
BEGIN_TEXT

What is the length of a tube that has a fundamental frequency of \($f1\)
and a first overtone of \($f2\) if the speed of sound is \($vw\)?
$PAR
\( l = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
First, formulate an equation which solves for the difference in frequency.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

Oooh, we're not told if this is an open or a closed tube,
so we'll have to be clever.  In both cases, the difference
between the fundamental and the first overtone is half the wavelength
for which the frequency is given by
$PAR
\( f = f_2 - f_1 = $f2 - $f1 = $f \)
$PAR
and substitute into
$PAR
\( \displaystyle l = \frac{v}{2 f} = \frac{$vw}{2 \times $f} = $L \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
