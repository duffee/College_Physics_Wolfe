## DESCRIPTION
# originally written by Caroline Promnitz and Sara Hesse, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
## ENDDESCRIPTION

## DBsubject(Electricity)
## DBchapter(Heat and Heat Transfer)
## DBsection(Speed Sound Frequency Wavelength)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('17.2')
## Problem1('7')
## MO(1)
## Static(1)
## KEYWORDS('speed','wavelength')

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$v = 343; # m/s
$TC = 20; # degC
$vf = 1540; # m/s
$vratio= Real($v / $vf);

ANS( $vratio->cmp );

Context()->texStrings;
BEGIN_TEXT

Dolphins make sounds in air and water.
What is the ratio of the wavelength of a sound in air to its wavelength in seawater?
Assume air temperature is  \($TC ^{\circ} \rm C\).
$PAR
\{ans_rule(40)\}

END_TEXT


BEGIN_HINT
Refer to chapter 17 in textbook to find speed of sound in sea water and air at \($TC ^{\circ} \rm C\).
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

ratio \( \displaystyle = \frac{$v}{$vf} = $vratio \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
