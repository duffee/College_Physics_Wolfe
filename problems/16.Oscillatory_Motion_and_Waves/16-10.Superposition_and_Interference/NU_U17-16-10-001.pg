##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(Superposition and Interference)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.10')
## Problem1('1')
## MO(1)
## KEYWORDS(frequency, interference)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$freq1 = NumberWithUnits( random(190, 199, 1), 'Hz');
$freq2 = NumberWithUnits( random(200, 209, 1), 'Hz');

$beat = $freq2 - $freq1;

ANS( $beat->cmp );


Context()->texStrings;
BEGIN_TEXT

A car has two horns, one emitting a frequency of \($freq1\)
and the other emitting a frequency of \($freq2\).
What beat frequency do they produce?
$PAR
\( f = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall beat frequency is defined as the difference in frequency between two similar-frequency waves.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( f_{beat} = f_2 - f_1 = $freq2 - $freq1 = $beat \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
