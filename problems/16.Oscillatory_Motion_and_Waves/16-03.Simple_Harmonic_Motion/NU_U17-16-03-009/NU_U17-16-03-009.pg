##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(Simple Harmonic Motion: A Special Periodic Motion)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.3')
## Problem1('9')
## MO(1)
## KEYWORDS(frequency, period, harmonic, Hooke's)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$mass1 = NumberWithUnits( random(80.00, 100.00, 0.1), 'kg');
$mass2 = NumberWithUnits( random(60.00, 80.00, 0.1), 'kg');
$T     = NumberWithUnits( random(1, 2, 0.01), 's');

$T2 = $T * sqrt(($mass1 + $mass2) / $mass1);

ANS( $T2->cmp );


Context()->texStrings;
BEGIN_TEXT
\{ image( 'Figure_16-47.png', width => 290, height => 300,
  tex_size=>700, extra_html_tags=>'alt="Skydivers (credit: U.S. Army, www.army.mil)"' ) \}

$PAR
A \($mass1\) skydiver hanging from a parachute bounces up and down
with a period of \($T\). What is the new period of oscillation
when a second skydiver, whose mass is \($mass2\),
hangs from the legs of the first?
$PAR
\( T_2 = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Can you find and equate two equivalent expressions given the spring constant,
both in terms of their respective period and mass?
Be careful - the second mass is not simply that of the second skydiver.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle T_2 = T_1 \sqrt{ \frac{m_1 + m_2}{m_1} }
  = ($T) \sqrt{ \frac{$mass1 + $mass2}{$mass1} }
  = $T2
\)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
