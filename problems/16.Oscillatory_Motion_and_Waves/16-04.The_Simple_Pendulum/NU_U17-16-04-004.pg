##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(The Simple Pendulum)
## Date(February 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.4')
## Problem1('4')
## MO(1)
## KEYWORDS(frequency, period, harmonic, Hooke's)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$g = NumberWithUnits( 9.81, 'm*s^-2');

$length = NumberWithUnits( random(2, 6, 0.1), 'm');
$period = NumberWithUnits( Real(2 * $PI) * sqrt($length / $g), 's');
ANS( $period->cmp );


Context()->texStrings;
BEGIN_TEXT

How long does it take a child on a swing to complete one swing
if her center of gravity is \($length\) below the pivot?
$PAR
\( T = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall the formula giving the period of a simple pendulum as a function of its length.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle T = 2 \pi \sqrt{ \frac{l}{g} }
  = 2 \pi \sqrt{ \frac{$length}{$g} }
  = $period
\)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
