##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(The Simple Pendulum)
## Date(February 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.4')
## Problem1('1')
## MO(1)
## KEYWORDS(frequency, period, harmonic, pendulum)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$g = NumberWithUnits( 9.81, 'm*s^-2');

$T = NumberWithUnits( random(0.300, 0.8, 0.01), 's');

$length = NumberWithUnits( $T**Real(2) * $g / Real(4 * $PI**2), 'm');
ANS( $length->cmp );


Context()->texStrings;
BEGIN_TEXT

What is the length of a pendulum that has a period of \($T\)?
$PAR
\( l = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall the formula giving the period of a simple pendulum as a function of its length.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle l = \frac{T^2 g}{4 \pi^2} = \frac{($T)^2($g)}{4 \pi^2} = $length \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
