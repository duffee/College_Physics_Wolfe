##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(The Simple Pendulum)
## Date(February 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.4')
## Problem1('5')
## MO(1)
## KEYWORDS(frequency, period, harmonic, Hooke's)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$g = NumberWithUnits( 9.81, 'm*s^-2');

$length = NumberWithUnits( random(0.02, 0.06, 0.001), 'm');
$frequency = NumberWithUnits( Real(1 /(2 * $PI)) / sqrt($length / $g), 'Hz');
ANS( $frequency->cmp );


Context()->texStrings;
BEGIN_TEXT

The pendulum on a cuckoo clock is \($length\) long. What is its frequency?
$PAR
\( f = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall the formula giving the frequency of a simple pendulum as a function of its length.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle f = \frac{1}{2 \pi} \sqrt{ \frac{g}{l} }
  = \frac{1}{2 \pi} \sqrt{ \frac{$g}{$length} }
  = $frequency
\)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
