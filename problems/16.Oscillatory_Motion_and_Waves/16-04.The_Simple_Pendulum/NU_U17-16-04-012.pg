##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(The Simple Pendulum)
## Date(February 2022)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.4')
## Problem1('12')
## MO(1)
## Static(1)
## KEYWORDS(frequency, period, harmonic)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$time = NumberWithUnits( 3600*24 * (1 - 1/sqrt(1.01)), 's');
ANS( $time->cmp );


Context()->texStrings;
BEGIN_TEXT

Suppose the length of a clock's pendulum is changed by an increase of \(1.000\%\),
exactly at noon one day. How slow will the clock be,
rounded to the nearest second, after \(24.00\) hours have passed,
assuming the pendulum has kept perfect time before the change?
$PAR
\( t = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Begin by finding the ratio of the new to old periods.
For every \(24.00\) hours that would have elapsed on the old pendulum, how many now elapse on the new, longer pendulum?
Is not the difference between these two rates the time by which (in hours, after one day) the pendulum runs slow?
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle t = 3600 \times 24 \times \left( 1 - \sqrt{ \frac{1}{1.01} } \right) = $time \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
