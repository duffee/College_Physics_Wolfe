##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(Waves)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.9')
## Problem1('1')
## MO(1)
## KEYWORDS(wave, velocity, kinematics)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$speed = NumberWithUnits( random(10, 20, 0.1), 'm*s^-1');
$distance = NumberWithUnits( 12E3, 'km');

$time = NumberWithUnits( $distance *Real(1E3) / $speed, 's');

ANS( $time->cmp );


Context()->texStrings;
BEGIN_TEXT

Storms in the South Pacific can create waves that travel all the way to the California coast,
which is \(12,000 \ \rm km\) away.
How long does it take them if they travel at \($speed\)?
$PAR
\( t = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall speed is defined as the change in distance divided by the change in time.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle t = \frac{d}{v} = \frac{$distance}{$speed} = $time \)
$PAR
which is about \{ sprintf("%.1f", $time->value / 86400) \} days.

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
