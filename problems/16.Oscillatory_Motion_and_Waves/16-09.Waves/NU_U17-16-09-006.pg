##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(Waves)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.9')
## Problem1('6')
## MO(1)
## KEYWORDS(wave, wavelength, frequency, velocity)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$speed = NumberWithUnits( random(0.5, 1.5, 0.01), 'm*s^-1');
$freq  = NumberWithUnits( random(1, 3, 0.01), 'Hz');

$lambda = NumberWithUnits( $speed / $freq, 'm');

ANS( $lambda->cmp );


Context()->texStrings;
BEGIN_TEXT

What is the wavelength of the waves you create in a swimming pool
if you splash your hand at a rate of \($freq\)
and the waves propagate at \($speed\)?
$PAR
\( \lambda = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
Recall the formula relating the velocity of a wave to its frequency and wavelength.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle \lambda = \frac{v}{f} = \frac{$speed}{$freq} = $lambda \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
