##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(Uniform Circular Motion and Simple Harmonic Motion)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.6')
## Problem1('4')
## MO(1)
## KEYWORDS(frequency, period, uniform, velocity)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;


$distance = NumberWithUnits( random(8, 20, 1), 'cm');
$rpm = Real( random(20, 50, 0.01) ); # rpm

$v_max = NumberWithUnits( $rpm * Real(2*$PI/60) * $distance, 'cm*s^-1');
ANS( $v_max->cmp );


Context()->texStrings;
BEGIN_TEXT

A ladybug sits \($distance\) from the center of a Beatles music album
spinning at \($rpm \ \rm rpm\).
What is the maximum velocity of its shadow on the wall behind the turntable,
if illuminated parallel to the record by the parallel rays of the setting Sun?
$PAR
\( v_{max} = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
What is the amplitude of the ladybug's motion?
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( \displaystyle v_{max} = 2 \pi \omega r
  = 2 \pi \frac{$rpm \ \rm rpm}{60} \times $distance
  = $v_max
\)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
