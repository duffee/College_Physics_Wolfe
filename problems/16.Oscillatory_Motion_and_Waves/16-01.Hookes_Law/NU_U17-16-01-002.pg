##DESCRIPTION
# originally written by Brandon Lostracco and Connor Wilson, Brock University, 2017-2018
# cleaned up, added solution and re-written to use NumberWithUnits
##ENDDESCRIPTION

## DBsubject(Mechanics)
## DBchapter(Oscillatory Motion and Waves)
## DBsection(Hooke's Law: Stress and Strain Revisited)
## Date(December 2021)
## Institution(Keele University)
## Author(Boyd Duffee)
## TitleText1('College Physics')
## AuthorText1('Wolfe et. al')
## EditionText1('2015')
## Section1('16.1')
## Problem1('2')
## MO(1)
## KEYWORDS(Equilibrium, Force, Hooke's, Law, Newton, Spring, Weight)

DOCUMENT();
loadMacros(
  'PGstandard.pl',
  'parserNumberWithUnits.pl',
);

TEXT(beginproblem());
Context("Numeric")->flags->set( tolerance => 0.005 );

$showPartialCorrectAnswers = 1;
$showHint = 3;

$g = NumberWithUnits( 9.81, 'm*s^-2');

$mass_max = NumberWithUnits( random(110, 120, 1), 'kg');
$x = NumberWithUnits( random(0.6, 0.8, 0.01), 'cm');
$k = NumberWithUnits( $mass_max * $g / ($x/Real(100)), 'N/m');
$x2 = NumberWithUnits( random(0.4, 0.5, 0.1), 'cm');
$m_player = NumberWithUnits( $k * $x2/Real(100) / $g, 'kg');

ANS( $k->cmp );
ANS( $m_player->cmp );


Context()->texStrings;
BEGIN_TEXT

It is weigh-in time for the local under-\(85\)-\(\textrm{kg}\) rugby team.
The bathroom scale used to assess eligibility can be described by Hooke's law
and is depressed \($x\) by its maximum load of \($mass_max\).
$PAR
a) What is the spring's effective spring constant?
$PAR
\( k = \) \{ans_rule(15)\}

$PAR
b) A player stands on the scales and depresses it by \($x2\). What is that players mass?
$PAR
\( m = \) \{ans_rule(15)\}

END_TEXT


BEGIN_HINT
For both (a) and (b), the rugby players compress the scale by its equilibrium distance when their weight and the spring force balance.
END_HINT

BEGIN_SOLUTION
$PAR $BBOLD SOLUTION $EBOLD $PAR

\( mg = kx \), so
$PAR
a)
$PAR
\( \displaystyle k = \frac{m g}{x} = \frac{($mass_max)($g)}{$x} = $k \)
$PAR
b)
$PAR
\( \displaystyle m_{player} = \frac{ k x }{g} = \frac{($k)($x2)}{$g} = $m_player \)

END_SOLUTION


COMMENT('Uses NumberWithUnits');
ENDDOCUMENT();
