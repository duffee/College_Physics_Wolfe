# Notes on how I lay out problem files

## space

Give argument and operators space, but tight to their enclosing brackets
random(3, 3.5, 0.05)
($activity_1uCi * $c * 60)*10E-6;

Group the ANS together and separate them from the variables by a blank line

## Units

add units in comments after variable assignments, unless already using NumberWithUnits

## Exponentials

prefer 1E-6 to 10**-6 to avoid the calculation

## Vertical space

Two blank lines between:
* metadata and DOCUMENT
* last ANS in problem setup and beginning problem text (Context()->texStrings; BEGIN_TEXT)
* END_TEXT and BEGIN_HINT/BEGIN_SOLUTION
* END_SOLUTION and COMMENT/ENDDOCUMENT
* ? maybe between $showHint and problem setup ?

No $PAR between BEGIN_TEXT and start of problem or end of problem and END_TEXT

## Indents
2 space indents

## latex

* prefer \mathrm{} to \textrm{} for units - better %
* use textrm when needing to preserve space in words

# Workflow
0. Start terminal session with `tmux new -s webwork`
1. Login and go to Library Browser, select problem set and Try It
2. tick Correct Answers and click Check Answers to get current correct answer
3. Enter correct answer and Check Answers to get Correct message
3. run `reformat problem_file.pg` to start changes
4. edit file
5. Check the answers haven't changed
