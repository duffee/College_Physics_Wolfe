# WebWork problems for College Physics for AP Courses

This is a collection of [WeBWorK](https://webwork.maa.org/)
problems for the OpenStax textbook,
[College Physics for AP Courses](https://openstax.org/details/books/college-physics-ap-courses)
by Wolfe et. al., 2015.

Originally started by Caroline Promnitz and Sara Hesse at Brock University, 2017-2018,
this project takes their work, cleans up the displayed text, adds solutions
and uses the
[NumberWithUnits](https://webwork.maa.org/wiki/ProblemsWithUnits)
macro to present a proper physics problem scenario.
While their work focused on the textbook, College Physics by Urone et.al. 2017,
the problems for the two textbooks are (so far) identical.
The intention is to follow through the WebWork group's procedure
to have these problems fully included in the
[Open Problem Library](https://github.com/openwebwork/webwork-open-problem-library).


## Licencing

This OpenStax textbook is licenced under the Creative Commons Attribution License v4.0

The OpenProblemLibrary, the source of the original problem files,
is licenced under a non-commercial use licence.

My advice if you want to use these problems commercially
is to approach Caroline Promnitz,
as I am willing to use the OpenStax licence terms.

# Important Reminder

Remember, kids: a number without a unit is either dimensionless or useless.
