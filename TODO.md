# List of Chapters

This is my todo list of chapters that I'm most interested in
at the moment.

* ~9 Statics and Torque~
* ~16.1 Hooke's Law~
* ~16.10 Superposition~
* ~20.3 Resistivity~
* ~17.2 SSFW -> progressive waves~
* ~33 Particle physics~
* ~27 Wave Optics (Interference)~
  * ~27.8 Polarization~
  * ~27.3 Two slit diffraction~
  * ~27.4 diffraction grating~
  * ~27.5 single slit diffraction~
  * ~27.1-2 interference, diffraction~
* ~25 Optics~
* 29 Quantum physics
* 21 Circuits
  * 21.2 Emf
* 19.6-8 Capacitors
* 5.3 Stress, Strain, Young's modulus

Zak's second semester

* ~11. Fluid mechanics~
* 13-15 Thermal physics SHC SLC heat transfer
* 16. Waves
* 31 Nuclear physics
* ~33 Particle physics~

And if I get time, I'll look at

* 3. Two-Dimensional Kinematics
* 2. Kinematics
* 16. Oscillations
* 31. Nuclear Physics
* 32. Medical Applications of Nuclear Physics
* 8. Momentum
* 7. Energy


## Issues noticed in authoring

### Units

* Need GeV, TeV, fm for Quarks
* Need keV for electric fields
* Use K as alias for degK
* move A from angstroms to amps, use 'ang' for angstroms, keep amp for backward compatibility

### Quarks

* a lot of static questions because particles have fixed energies
  * combine to make one question asking about different particles
* a number of "verify this is conserved" questions assume an order
* quark composition is using fun_cmp could be moved to Formula
* how to write antiparticles? Idea from Brock, suggest using capitals, uU dD

### Authoring questions

* how to accept two NumberWithUnits answers in any order (what are the 2 wavelengths?)
* how to specify an integer answer

### Wishlist

* can we have 10^15 without the 1 \times?
* be nice to choose sig figs in sci notation  1.50 \times 10^2
* change font/size of MathJax images
* ~easily remove units from calculations Real( $length / $time ) has unit of 'm'~
  * solution is to place a Real() as the first item in the assignment
* display tex symbols for units to get degrees, ohms, micrometres
