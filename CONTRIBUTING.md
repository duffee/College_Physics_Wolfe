# Welcome

It takes me a month to get through a chapter, so thank you for contributing.

# Git tricks

## Getting started

TODO: verify these instructions

* fork the project on Gitlab
* clone your forked project to your local machine
* add original project as `upstream`

  git remote add upstream https://github.com/duffee/College_Physics_Wolfe.git

* push changes to your forked project

  git push

* create a Merge Request for adoption into the original project

* pull changes from the original project

  git pull upstream main

## Branches

As more people contribute, we'll have to discuss branches
so that merging commits is managable.

# Style

The rationale is to make it easy to find sections of code
and for your intention to be understood.  It makes it easier
to fix bugs.
I'm starting to develop a certain style with 2 newlines
separating sections and 1 newline separating paragraphs of code.
I'm trying to keep to a 90 column width maximum as much as possible,
but breaking at punctuation is preferred.

Use `NumberWithUnits` as much as possible because it displays
the unit correctly in the problem text with `\($height\)`

Try to use sensibly descriptive variable names for the next person
to understand the intention of your code.

Aim for a minimum Solution of formula, substitution, answer.
i.e. `\( F = m a = ($mass)($acceleration) = $force \)`
Explanitory notes are a big win for students and future authors.

A problem with an image goes in its own directory for portability.

The value for `g` goes at the top of the code, so that instructors
know whether this problem uses 9.8 or 9.81.

I use a relative tolerance of 0.005 so that answers to 2 sig figs are correct.

A git hook enforces a "no trailing whitespace" policy.  If you try
to commit a file with whitespace at the end of a line, it will
abort the commit and show you where the offending spaces are.
Don't forget to add the file again after you remove them.
It is a small thing, but they do take up unneeded space.

There are some perltidy styles that I prefer for readability
* whitespace around operators and between list items
  * but no space around unit conversions
* 2 space indents



## Still making up my mind

I've started with `ansrule(15)` for a shorter answer box,
but I could be convinced to lengthen it with a good reason.

I like one big TEXT section, but Promnitz has broken them
up with individual HINT sections.  I've left it like that for now.
